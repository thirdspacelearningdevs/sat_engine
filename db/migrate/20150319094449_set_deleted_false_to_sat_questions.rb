class SetDeletedFalseToSatQuestions < ActiveRecord::Migration
  def up
    SatEngine::SatQuestion.find_each do |question|
      question.update_attributes(deleted: false)
    end
  end

  def down
    SatEngine::SatQuestion.find_each do |question|
      question.update_attributes(deleted: nil)
    end
  end
end