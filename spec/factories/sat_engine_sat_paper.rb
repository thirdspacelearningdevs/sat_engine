FactoryGirl.define do
  factory :sat_engine_sat_paper, :class => 'SatEngine::SatPaper' do
    user_id 1
    last_selected_navigation_tab "curriculum"
    finished false
    pdf_path nil
    deleted false
    deleted_at nil
  end

end
