class AddDeletedToGranuleSatQuestionAlternative < ActiveRecord::Migration
  def change
    add_column :sat_engine_granule_sat_question_alternatives, :deleted, :boolean, default: false
    add_column :sat_engine_granule_sat_question_alternatives, :deleted_at, :datetime, after: :deleted
  end
end
