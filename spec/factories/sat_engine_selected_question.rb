FactoryGirl.define do
  factory :sat_engine_selected_question, :class => 'SatEngine::SelectedQuestion' do
    title "Centimeters"
    sat_paper_id 1
    sat_question_id 1
    position 1 
  end

end
