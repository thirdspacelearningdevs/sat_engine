module SatEngine
  class ApplicationController < ActionController::Base
    layout 'layouts/application' 

    def authenticate_tutor_or_tutor_admin_or_admin_or_academic_councillor!
      redirect_to main_app.home_path unless current_user.academic_councillor? or current_user.tutor_admin? or current_user.admin? or current_user.tutor?
    end

    def authenticate_admin!
      redirect_to main_app.home_path unless current_user.admin?
    end

  end
end
