require 'CSV'
require "spec_helper"

describe SatEngine::SatUploadsController do
  routes { SatEngine::Engine.routes }

  before(:each) do
    sign_in create_user(:admin)
  end
  
  describe "GET #new" do
    it "assigns a new Upload attempt to @sat_upload" do
      get :new
      expect(assigns(:sat_upload)).to be_a_new(SatEngine::SatUpload)
    end
    it "renders the :new template" do
      get :new
      expect(response).to render_template :new
    end
  end
  
  describe "POST #create" do
    context "with valid data" do
      it "saves the new Upload attempt to the database" do
        expect{
          post :create, { sat_upload: { csv_file: Rack::Test::UploadedFile.new(File.join(Rails.root, 'test.csv')) }}
        }.to change(SatEngine::SatUpload, :count).by(1)
      end
      it "it redirects to show action" do
        post :create, { sat_upload: { csv_file: Rack::Test::UploadedFile.new(File.join(Rails.root, 'test.csv')) }}
        expect(response).to redirect_to(sat_upload_path(assigns(:sat_upload)))
      end
    end
    context "without csv file" do
      after(:each) do
        FileUtils.rm_rf(Rails.root.join('public', 'uploads', 'sat_engine', 'sat_uploads').to_s)
      end
      it "does not save the new Upload attempt in the database" do
        expect{
          post :create, { sat_upload: { csv_file: ""} }
        }.to_not change(SatEngine::SatUpload, :count)
      end
      it "renders the :new template" do
        post :create, { sat_upload: { csv_file: ""} }
        expect(response).to render_template :new
      end
    end
  end

  describe "GET #load_sat_questions" do
    context "with valid csv" do
      before(:each) do
        @sat_upload = FactoryGirl.create(:sat_engine_sat_upload)
      end

      it "upload sat questions" do
        expect{
          get :load_sat_questions, id: @sat_upload
        }.to change(SatEngine::SatQuestion, :count).by(3)
      end

      it "upload sat's alternative granules" do
        expect{
          get :load_sat_questions, id: @sat_upload
        }.to change(SatEngine::GranuleSatQuestionAlternative, :count).by(5)
      end

      it "redirects to action show" do
        expect{
          get :load_sat_questions, id: @sat_upload
        }.to change(SatEngine::GranuleSatQuestionAlternative, :count).by(5)

        expect(response).to redirect_to(new_sat_paper_url)
      end

      it "destroy all existing sat questions and question alternatives" do
        sat_question = create(:sat_engine_sat_question)
        sat_question_alternative = create(:granule_sat_question_alternative)
        get :load_sat_questions, id: @sat_upload
        sat_question.reload
        sat_question_alternative.reload

        expect(sat_question.deleted).to be true
        expect(sat_question_alternative.deleted).to be true
      end
    end

    context "with invalid valid csv" do
     before(:each) do
        @sat_upload = FactoryGirl.create(:sat_engine_sat_upload_invalid)
      end

      it "do not upload sat questions" do
        expect{
          get :load_sat_questions, id: @sat_upload
        }.to change(SatEngine::SatQuestion, :count).by(0)
        expect(flash[:error]).not_to be_nil
      end
    end
  end

end

