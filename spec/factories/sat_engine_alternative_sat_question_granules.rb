FactoryGirl.define do
  factory :granule_sat_question_alternative, :class => 'SatEngine::GranuleSatQuestionAlternative' do
    granule_position 2
		sat_question_id 1
  end

end
