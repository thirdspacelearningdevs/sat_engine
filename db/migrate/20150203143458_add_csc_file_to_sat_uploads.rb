class AddCscFileToSatUploads < ActiveRecord::Migration
  def up
    add_column :sat_engine_sat_uploads, :csv_file, :string
  end

  def down
    remove_column :sat_engine_sat_uploads, :csv_file
  end
end
