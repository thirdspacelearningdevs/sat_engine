module SatEngine
  class GranuleSatQuestionAlternative < ActiveRecord::Base
    attr_accessible :granule_position, :sat_question_id, :deleted, :deleted_at

    belongs_to :alternative_sat_questions, class_name: SatEngine::SatQuestion.to_s, foreign_key: :sat_question_id
    belongs_to :granule, foreign_key: :granule_position, primary_key: :position

    scope :available, -> { where(:deleted => false) }

    def self.mark_as_destroyed_all!
      GranuleSatQuestionAlternative.update_all(deleted: true, deleted_at: Time.zone.now)
    end

    def mark_as_destroyed!
      update_attribute(:deleted, true)
      update_attribute(:deleted_at, Time.zone.now)
    end

  end
end