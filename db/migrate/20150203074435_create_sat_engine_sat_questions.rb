class CreateSatEngineSatQuestions < ActiveRecord::Migration
  def change
    create_table :sat_engine_sat_questions do |t|
      t.integer :year
      t.string :papers
      t.string :question
      t.integer :marks
      t.string :question_code
      t.string :mark_scheme_code
      t.integer :level
      t.integer :granule_position
      t.integer :alternative_granule_position

      t.timestamps
    end
  end
end
