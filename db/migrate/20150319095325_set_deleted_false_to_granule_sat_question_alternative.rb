class SetDeletedFalseToGranuleSatQuestionAlternative < ActiveRecord::Migration
  def up
    SatEngine::GranuleSatQuestionAlternative.find_each do |question|
      question.update_attributes(deleted: false)
    end
  end

  def down
    SatEngine::GranuleSatQuestionAlternative.find_each do |question|
      question.update_attributes(deleted: nil)
    end
  end
end