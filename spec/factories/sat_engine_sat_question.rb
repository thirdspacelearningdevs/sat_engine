FactoryGirl.define do
  factory :sat_engine_sat_question, :class => 'SatEngine::SatQuestion' do
    year 2003
		paper "A"
		question "1a"
		marks 1
		question_code "SAT0_03_A_1a_Q"
		mark_scheme_code "SAT0_03_A_1a_M"
    paper_level 0
  end

end
