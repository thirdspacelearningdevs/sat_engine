var Sat = $.inherit({
  __constructor : function() {
        $('#sat_curriculum_navigation_link').livequery('click',$.delegate(this, this.satCurriculumNavigation));
        $('#sat_search_granule_text').livequery('keydown',$.delegate(this, this.satCurriculumNavigationSearch));
        $('#sat_curriculum_navigation_search_button').livequery('click',$.delegate(this, this.satCurriculumNavigationSearchByButton));
        $('#sat-granule-table .sat-granule-pagination a').livequery('click',$.delegate(this, this.satGranulePagination));
        $('.sat-filter-granule').livequery('change',$.delegate(this, this.satFilterGranuleBySelect));
        $('#sat-granule-table .sat-main-granule-pagination a').livequery('click',$.delegate(this, this.satMainGranulePagination));
    },


  satCurriculumNavigation: function(src){
    $.get(src, function(data) {
      $(".sat_curriculum_navigation").empty();
      $(".sat_curriculum_navigation").append(data);
    });
    return false;
  },
  
  satCurriculumNavigationSearchByButton: function(src){
    var url = src;
    var search_text = $("#sat_search_granule_text").val();
    var address = url + "?search=" + search_text;
    $.get(address, function(data) {
      $("#sat-granule-table").empty();
      $("#sat-granule-table").append(data["content"]);
    }, "json");
    return false;
  },
  
  satCurriculumNavigationSearch: function(src, event){
    if(event.keyCode === 13) {
        var url = $("#sat_curriculum_navigation_link").attr("href");
        var search_text = $("#sat_search_granule_text").val();
        var address = url + "?search=" + search_text;
        $.get(address, function(data) {
          $("#sat-granule-table").empty();
          $("#sat-granule-table").append(data["content"]);
        }, "json");
        return false;
    }
  },
  
  satGranulePagination: function(src) {
    $.get(src, function(data) {
      $("#sat-granule-table").empty();
      $("#sat-granule-table").append(data["content"]);
    }, "json");
    return false; 
  },
  
  satFilterGranuleBySelect: function(src) {
    var url = $(src).data('href');
    var filter_by = $(src).attr("id");
    var selected_value = $(src).val();
    var address = url + "?filter=" + filter_by + "&value=" + selected_value;
    $.get(address, function(data) {
      var select_list = $("select[id!=" + filter_by + "].sat-filter-granule");
      $.each(select_list, function( index, value ) {
        $(value).val(value[0]);
      });
      $("#sat-granule-table").empty();
      $("#sat-granule-table").append(data["content"]);
    }, "json");
    return false;
  },
  
  satMainGranulePagination: function(src) {
    var page_string = "?page=" + $(src).text();
    var url = $(".sat-main-granule-pagination").data('href');
    $.get(url + page_string, function(data) {
      $(".sat_curriculum_navigation").empty();
      $(".sat_curriculum_navigation").append(data["content"]);
    }, "json");
    return false; 
  },

});

jQuery(document).ready(function() {
  new Sat();
});

