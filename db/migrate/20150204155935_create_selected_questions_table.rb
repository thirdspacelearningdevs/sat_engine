class CreateSelectedQuestionsTable < ActiveRecord::Migration
  def change
    create_table :sat_engine_selected_questions do |t|
      t.string :title
      t.integer :sat_paper_id
      t.integer :sat_question_id
      t.integer :position
      t.timestamps
    end
  end
end
