class CreateSatUploadTable < ActiveRecord::Migration
  def change
    create_table :sat_engine_sat_uploads do |t|
      t.boolean :finished, :default => false
      t.timestamps
    end
  end
end
