require "spec_helper"
require "shoulda-matchers"

describe SatEngine::SelectedQuestion do
  subject { FactoryGirl.build(:sat_engine_selected_question) }
  it { expect(subject).to be_valid }
  it { expect(subject).to belong_to(:sat_paper) }
  it { expect(subject).to belong_to(:sat_question) }
end
