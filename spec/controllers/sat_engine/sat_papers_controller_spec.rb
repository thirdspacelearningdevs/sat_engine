require "spec_helper"

describe SatEngine::SatPapersController do
  routes { SatEngine::Engine.routes }
  
  describe "GET #new" do
    before(:each) do
      sign_in_user create_user('tutor')
    end
    it "assigns a new Sat paper to @sat_paper" do
      get :new
      expect(assigns(:sat_paper)).to be_a_new(SatEngine::SatPaper)
    end
    it "renders the :new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "GET #show" do
    before(:each) do
      sign_in_user create_user('tutor')
      @sat_paper = FactoryGirl.create(:sat_engine_sat_paper)
      @tsl_token = Settings.thirdspacelearning_token
      @url = "#{Settings.third_space_api_url}sat_engine/sat_papers/#{@sat_paper.id}"
    end

    context "with successful response from API" do
      before(:each) do
        @paper_pdf_url = "http://s3-eu-west-1.amazonaws.com/thirdspacelearning-staging/uploads/thirdspacelearning/development/sat_engine/sat_paper/file/341/341_paper.pdf"

        stub_request(:get, @url).
          with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
           'Authorization'=>"Token token=#{@tsl_token}", 'User-Agent'=>'Ruby'}).
          to_return(status: 200, body: { success: true, url: @paper_pdf_url }.to_json, headers: {})
      end

      it 'recives path to Sat Paper file from API' do 
        get :show, { id: @sat_paper.id }
        expect(assigns(:sat_paper_url)).to eq @paper_pdf_url
        expect(assigns(:sat_paper)).to eq @sat_paper
      end
      it 'renders :show template' do
        get :show, { id: @sat_paper.id }
        expect(response).to render_template :show
      end
    end

    context "with false response from API" do
      before(:each) do
        stub_request(:get, @url).
          with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
           'Authorization'=>"Token token=#{@tsl_token}", 'User-Agent'=>'Ruby'}).
          to_return(status: 200, body: { success: true, url: @paper_pdf_url }.to_json, headers: {})
          Rails.application.routes.url_helpers.stub(:home_path).and_return('home_path')
      end

      it "redirect to home page with error message" do
        get :show, { id: @sat_paper.id }
        expect(response).to redirect_to('home_path')
        expect(flash[:error]).not_to be_nil
      end
    end

  end
end

