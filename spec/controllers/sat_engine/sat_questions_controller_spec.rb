require 'spec_helper'

describe SatEngine::SatQuestionsController do
  routes { SatEngine::Engine.routes }
  subject { FactoryGirl.create(:sat_engine_sat_question) }

  describe "#GET #preview" do
    context "as anonymous visitor" do

      it "redirect to home page" do
        get :preview, { id: subject.id }
        expect(response).to redirect_to new_user_session_path
      end
    end

    context "as tutor" do
      before(:each) do
        @granule = FactoryGirl.create(:granule)
        sign_in create_user(:tutor)
      end

      it "render :preview" do
        get :preview, { id: subject.id, granule_name: @granule.name, format: :json }
        expect(response).to render_template :view
      end

      it "assings granule name" do
        get :preview, { id: subject.id, granule_name: @granule.name, format: :json }
        expect(assigns(:granule_name)).to eq @granule.name
      end

      it "assings Sat Question" do
        get :preview, { id: subject.id, granule_name: @granule.name, format: :json }
        expect(assigns(:sat_question)).to eq subject
      end
    end
  end

  describe "#GET #questions_for_granule" do
    context "as anonymous visitor" do

      it "redirect to home page" do
        get :questions_for_granule, { granule_position: 1 }
        expect(response).to redirect_to new_user_session_path
      end
    end
    context "as tutor" do
      before(:each) do
        @granule = FactoryGirl.create(:granule, position: 2)
        subject.update_attribute(:granule_position, @granule.position)
        subject.reload

        sign_in create_user(:tutor)
      end

      it "render partial questions_for_granule" do
        get :questions_for_granule, { granule_position: @granule.position, format: :json }
        expect(response).to render_template(:questions_for_granule)
      end

    end
  end

  describe "#GET show_image" do
    context "as anonymous visitor" do

      it "redirect to home page" do
        get :show_image, { id: subject.id }
        expect(response).to redirect_to new_user_session_path
      end
    end

    context "as tutor" do
      before(:each) do
        @granule = FactoryGirl.create(:granule)
        sign_in create_user(:tutor)
      end

      context "with specified image type" do
        it "sends Sat Question question image when image type 'question'" do
          get :show_image, { id: subject.id, type: 'question' }
          expect(response.content_type).to eq 'image/png'
          expect(assigns(:image)).to eq subject.question_file
        end

        it "sends Sat Question mark scheme image when image type 'mark_scheme'" do
          get :show_image, { id: subject.id, type: 'mark_scheme' }
          expect(response.content_type).to eq 'image/png'
          expect(assigns(:image)).to eq subject.mark_scheme_file
        end
      end

      context "without type" do
        it "send no image" do
          get :show_image, { id: subject.id }
          expect(assigns(:image)).to be_nil
        end
      end
    end

  end
end