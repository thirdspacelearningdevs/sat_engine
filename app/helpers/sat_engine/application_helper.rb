module SatEngine
  module ApplicationHelper

  	def pluralize count, noun
  		if count != 0
		    count == 1 ? "#{count} #{noun}" : "#{count} #{noun.pluralize}"
		  end
  	end

  	def shortened_granule_name name
			return if name.nil?
			if name.length > 24
				return name[0..24] + '...'
			else
				return name
			end
		end

    # TODO Extract to separate class
		def rescale_sat_question_image image_size
      # max sizes are defined according to
      # pdf landscape and positioning elements on it
      max_paper_height = 550
      max_paper_width = 600

      tall_img_max_width = 625
      short_img_constrain = 390
      ################################################

      width = image_size[0]
      height = image_size[1]
      landscape_image = landscape_image?(width, height)

      ratio = image_ratio(width, height, landscape_image)
      scaled_height = rescale_height(max_paper_width, ratio, landscape_image)

      # image is short - use whole page width
      if scaled_height < short_img_constrain
        new_height = max_paper_height
        new_width = rescale_width(max_paper_height, ratio, landscape_image)

        # height is fine - but width doesn't fit
        if new_width > max_paper_width
          new_width = max_paper_width
          new_height = rescale_height(new_width, ratio, landscape_image)
        end
      else # image is tall - make space for marks
        new_width = tall_img_max_width
        new_height = rescale_height(new_width, ratio, landscape_image)

        if new_height > max_paper_height
          new_height = max_paper_height
          new_width = rescale_width(new_height, ratio, landscape_image)
        end
      end

      { width: new_width, height: new_height }
    end

   def rescale_mark_scheme_image image_size
      # max sizes are defined according to
      # pdf landscape and positioning elements on it
      max_mark_scheme_height = 445
      max_mark_scheme_width = 855

      width = image_size[0]
      height = image_size[1]
      ###############################################

      landscape_image = landscape_image?(width, height)
      ratio = image_ratio(width, height, landscape_image)
      scaled_height = rescale_height(max_mark_scheme_height, ratio, landscape_image)
      scaled_width = rescale_width(scaled_height, ratio, landscape_image)

      if height > scaled_height
        new_height = max_mark_scheme_height
        new_width = rescale_width(new_height, ratio, landscape_image)

        if new_width > max_mark_scheme_width
          new_width = max_mark_scheme_width
          new_height = rescale_height(new_width, ratio, landscape_image)
        end
      else
        new_width = scaled_height
        new_height = scaled_width
      end

      { width: new_width, height: new_height }
    end

    def image_ratio width, height, landscape_orientation
      if landscape_orientation
        (width.to_f / height.to_f)
      else
        (height.to_f / width.to_f)
      end
    end

    def rescale_width height, ratio, landscape_orientation
      if landscape_orientation
        width = height * ratio
      else
        width = height / ratio
      end
      width
    end

    def rescale_height width, ratio, landscape_orientation
      if landscape_orientation
        height = width / ratio
      else
        height = width * ratio
      end
      height
    end

    def landscape_image? width, height
      width > height
    end

    def sat_question_page_index index
      return 1 if index == 0
      (index * 2 + 1)
    end

    def paper_pdf_link url
      url.present? ? url : '#'
    end

    def disabled_pdf_link? url
       url.nil? ? 'disabled' : nil
    end

    def local_request?
      Rails.env.development? or request.remote_ip =~ /(::1)|(127.0.0.1)|((192.168).*)/
    end

    def set_body_class
      params[:controller].to_s+'-'+params[:action].to_s
    end

    def logo_url
      return main_app.calendar_path if current_user.present? and current_user.tutor?
      main_app.home_path
    end

    def set_nav_current_item when_selected_array
      return 'current-item' if when_selected_array.include?(params[:controller])
      ''
    end

    def set_current_item_by_action when_selected_array
      return 'current-item' if when_selected_array.include?(params[:action])
      ''
    end

    def set_current_item_by_controller_and_action when_selected_hash
      if when_selected_hash[:controller] == params[:controller] and when_selected_hash[:action] == params[:action]
        return 'current-item'
      end
      ''
    end

    def set_current_item_by_controller_and_action_by_array when_selected_hash_array
      when_selected_hash_array.each do |when_selected_hash|
        if when_selected_hash[:controller] == params[:controller] and when_selected_hash[:action] == params[:action]
          return 'current-item'
        end
      end
      ''
    end

    def reports_dropdown_hash_array
      [
        {controller: "reports", action: 'learning_objective'},
        {controller: "reports", action: 'student_performance'},
        {controller: "reports", action: 'student_details'},
        {controller: "reports", action: 'weekly_tutor'},
        {controller: "reports", action: 'weekly_school'},
        {controller: "reports", action: 'sittings'},
        {controller: "reports", action: 'authorities_schools'},
        {controller: "reports", action: 'school_map'},
        {controller: "reports", action: 'school_setup_progress'},
        {controller: "learning_session_reassignments", action: 'index'},
        {controller: "delayed_exports", action: 'index'}
      ]
    end

    def accounts_dropdown_hash_array
      [
        {controller: "accounts", action: 'review'},
        {controller: "teachers", action: 'index'},
        {controller: "learning_groups", action: 'index'},
        {controller: "tutors", action: 'index'},
        {controller: "tutor_admins", action: 'index'},
        {controller: "supply_tutors", action: 'index'}
      ]
    end

    def curriculum_dropdown_hash_array
      [
        {controller: "curriculum", action: 'index'},
        {controller: "granules", action: 'index'},
        {controller: "reports", action: 'granule_history'},
        {controller: "granules", action: 'new'}
      ]
    end

    def sessions_dropdown_hash_array
      [
        {controller: "learning_sessions", action: 'completed'},
        {controller: "learning_sessions", action: 'sessions_tracker'},
        {controller: "learning_sessions", action: 'upcoming'}
      ]
    end

    def contracts_dropdown_hash_array
      [
        {controller: "contracts", action: 'add'},
        {controller: "contracts", action: 'index'}
      ]
    end

    def tutor_sessions_dropdown_hash_array
      [
        {controller: "learning_sessions", action: 'completed'},
        {controller: "calendar", action: 'index'},
        {controller: "devise_controllers/users", action: 'individual_kosmos_sessions'}
      ]
    end

    def error_message_builder object
      content_tag :div, :id => 'errorExplanation' do
        content_tag :ul do
          content = ''
          object.errors.full_messages.each do |msg|
            content += content_tag :li, msg, :class => 'error'
          end
          raw content
        end
      end
    end

    def date_format time, format = :long
    case format
    when :full then time.strftime('%d-%m-%Y %I:%M%p')
    end
  end

  end
end
