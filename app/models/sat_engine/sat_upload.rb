require 'carrierwave'
require 'carrierwave/mount'

module SatEngine
  class SatUpload < ActiveRecord::Base
    SAT_PAPER_TOTAL_COLUMNS = 14

    attr_accessible :csv_file, :finished
    validates :csv_file, presence: true
    mount_uploader :csv_file, CsvUploader

    def load_sat_questions
      destroy_existing_questions!

      CSV.foreach(csv_file.file.path, { :headers => true }) do |row|
        question = load_sat_question(row.to_hash)
        load_alternative_sat_qustion_granules(row, question)
        question.save
      end
    end

    def load_sat_question row
      question = SatQuestion.new do |q|
        q.year = row[sat_q_header_year]
        q.paper = row[sat_q_header_paper]
        q.question = row[sat_q_header_questions]
        q.marks = row[sat_q_header_marks]
        q.question_code = row[sat_q_header_code_question]
        q.mark_scheme_code = row[sat_q_header_code_mark_scheme]
        q.level = row[sat_q_header_level]
        q.granule_position = row[sat_q_header_best_match]
        q.paper_level = row[sat_q_paper_level]
      end
      question
    end

    def load_alternative_sat_qustion_granules row, question
      row.fields[8..12].compact.each do |granule_position|
        unless granule_position.nil?
          question.granule_sat_question_alternatives.build(granule_position: granule_position)
        end
      end
    end

    def validate_sat_question_headers
      errors = []
      csv_header = CSV.read(csv_file.file.path).first
      errors << "invalid number of columns in csv should be #{SAT_PAPER_TOTAL_COLUMNS} is #{csv_header.count}" unless csv_header.count == SAT_PAPER_TOTAL_COLUMNS
      header_missing = []
      expected_csv_sat_q_header.each do |header|
         header_missing << header unless csv_header.include? header
      end
      errors << "required headers were not found: #{header_missing.join(', ')}" unless header_missing.blank?

      { success: (errors.blank? ? true : false), errors: errors }
    end

    def sat_q_header_year; 'Year'; end
    def sat_q_header_paper; 'Paper'; end
    def sat_q_header_questions; 'Questions'; end
    def sat_q_header_marks; 'marks'; end
    def sat_q_header_code_question; 'Unique Code Question'; end
    def sat_q_header_code_mark_scheme; 'Unique Code Mark Scheme'; end
    def sat_q_header_level; 'Level'; end
    def sat_q_header_best_match; 'Best match'; end
    def sat_q_paper_level; 'Paper Level'; end

    def valid_csv_file
      return false if csv_file.file.nil?
      return false if csv_file.file.path.nil?
      true
    end

    private
    def destroy_existing_questions!
      SatQuestion.mark_as_destroyed_all!
      GranuleSatQuestionAlternative.mark_as_destroyed_all!
    end

    def expected_csv_sat_q_header
      [sat_q_header_year, sat_q_header_paper, sat_q_header_questions,
       sat_q_header_marks, sat_q_header_code_question, sat_q_header_code_mark_scheme, 
       sat_q_header_level, sat_q_header_best_match, sat_q_paper_level
      ]
    end

  end
end


