$:.push File.expand_path("../lib", __FILE__)
require "sat_engine/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "sat_engine"
  s.version     = SatEngine::VERSION
  s.authors     = "TSL"
  s.email       = "info@thirdspacelearning.com"
  s.homepage    = "http://thirdspacelearning.com/"
  s.summary     = "SAT Engine"
  s.description = "SAT Engine"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", "~> 4"
  s.add_dependency "mysql2", "~> 0.3.15"

  # Settings.yml config files
  s.add_dependency "rails_config"
  s.add_dependency "wicked_pdf"

  s.add_development_dependency "rspec-rails", "2.14.2"
  s.add_development_dependency "capybara"
  s.add_development_dependency "factory_girl_rails"
  s.add_development_dependency "faker", "1.3.0"
  s.add_development_dependency "shoulda-matchers"
  s.add_development_dependency "shoulda-callback-matchers", "1.1.2"
  s.add_development_dependency "database_cleaner", "1.2.0"

  s.add_development_dependency "prawn"
  s.add_development_dependency "prawn-table"
  s.add_development_dependency "awesome_print"
  s.add_development_dependency "pry"
  s.add_development_dependency "pry-rails"
  
  s.add_development_dependency 'carrierwave'
  s.add_development_dependency 'fog'
  s.add_development_dependency 'rmagic'
  s.add_development_dependency 'devise'
  s.add_development_dependency 'webmock'
  s.add_development_dependency 'timecop'
end
