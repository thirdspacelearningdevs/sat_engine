module SatEngine
  module GranuleHelper
    def display_topic_name granule
      if granule.nil?
        "-"
      else
        granule.full_name
      end
    end
    
    def granule_year_select
      a = CurriculumLevel.
        available.
        select("curriculum_levels.name").
        uniq("curriculum_levels.name").
        order("curriculum_levels.name").map do |level|
          [level.name[4..-1], level.name]
      end
      a.unshift("-")
      return a
    end
    
    def granule_category_select
      a = CurriculumCategory.
        available.
        select("curriculum_categories.name").
        uniq("curriculum_categories.name").
        order("curriculum_categories.name").map do |category|
          [category.name, category.name]
      end
      a.unshift("-")
      return a
    end
    
    def granule_topic_select
      a = CurriculumTopic.
        available.
        select("curriculum_topics.name").
        uniq("curriculum_topics.name").
        order("curriculum_topics.name").map do |topic|
          [topic.name, topic.name]
      end
      a.unshift("-")
      return a
    end
    
    def granule_level_select
      a = Granule.available.select("DISTINCT level").order(:level).map do |granule|
        [granule.level, granule.level]
      end
      a.unshift("-")
      return a
    end
    
    def highlight_granule_search_term text, term
      if term.nil?
        text
      else
        text.gsub(/(#{term})/i) { |t| "<b class='red-link'>#{t}</b>" }.html_safe
      end
    end
  end
end