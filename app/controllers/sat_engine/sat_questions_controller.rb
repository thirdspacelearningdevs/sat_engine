  module SatEngine
    class SatQuestionsController < ApplicationController
    before_filter :authenticate_user!
    before_filter :authenticate_tutor_or_tutor_admin_or_admin_or_academic_councillor!

    def preview
      @granule_name = params[:granule_name]
      @sat_question = SatQuestion.find(params[:id])
      respond_to do |format|
        format.json { render :json => {:success => true, :content => render_to_string(:action => :preview) } }
      end
    end

    def questions_for_granule
      all_granule_sat_questions_hash = SatQuestion.all_questions_for_granule_hash(params[:granule_position])

      respond_to do |format|
        format.html { flash[:error] = "Invalid request"; redirect_to home_path }
        format.json { render :json => {:success => true, :content => render_to_string(partial: 'questions_for_granule', locals: { sat_questions: all_granule_sat_questions_hash })} }
      end
    end

    def show_image
      @sat_question = SatQuestion.find(params[:id])
      @image = question_file_from_type(@sat_question, params[:type])

      send_data @image, :type => 'image/png', :disposition => 'inline'
    end

    private
    def question_file_from_type sat_question, type
      case type.to_s
      when 'question' then return @sat_question.question_file
      when 'mark_scheme' then return @sat_question.mark_scheme_file
      end
    end

  end
end