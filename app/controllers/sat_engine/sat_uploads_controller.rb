module SatEngine
  class SatUploadsController < SatEngine::ApplicationController
    before_filter :authenticate_user!
    before_filter :authenticate_admin!
    before_filter :validate_sat_questions_csv!, :only => [:load_sat_questions]

    def new
      @sat_upload = SatUpload.new
    end
    
    def create
      @sat_upload = SatUpload.new(params[:sat_upload])
      if @sat_upload.save
        @sat_upload.update_attribute(:finished, true)
        flash[:success] = I18n.t('sat_uploads.success')
        redirect_to sat_upload_path(@sat_upload)
      else
        flash[:error] = I18n.t('sat_uploads.error')
        render :action => :new
      end
    end
    
    def show
      @sat_upload = SatUpload.find(params[:id])
    end

    # TODO: Move flash to EN file
    def load_sat_questions
      @sat_upload = SatUpload.find(params[:id])
      @sat_upload.load_sat_questions
      flash[:success] = I18n.t('sat_uploads.load_sat_questions.success')
      redirect_to new_sat_paper_url
    end

    protected
    def validate_sat_questions_csv!
      sat_upload = SatUpload.find(params[:id])
      if sat_upload.valid_csv_file
        headers_validation_result = sat_upload.validate_sat_question_headers
        if (headers_validation_result[:success])
          return true
        else
          flash[:error] = I18n.t('sat_uploads.load_sat_questions.error.list_errors', errors: headers_validation_result[:errors].join('; '))
          redirect_to action: :show
        end
      else
        flash[:error] =  I18n.t('sat_uploads.load_sat_questions.error.validation_error')
        redirect_to action: :show
      end
    end

  end
end