class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
           :recoverable, :rememberable, :trackable, :validatable, :confirmable

  def admin?
    self.is_a? Admin
  end

  def tutor?
    self.is_a? Tutor
  end

  def academic_councillor?
    self.is_a? AcademicCouncillor
  end

  def tutor_admin?
    self.is_a? TutorAdmin
  end
end