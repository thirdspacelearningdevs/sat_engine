FactoryGirl.define do
  factory :sat_engine_sat_upload, :class => 'SatEngine::SatUpload' do
    csv_file { Rack::Test::UploadedFile.new(File.join(Rails.root, 'test.csv')) }
    finished true
  end

  factory :sat_engine_sat_upload_invalid, :class => 'SatEngine::SatUpload' do
    csv_file { Rack::Test::UploadedFile.new(File.join(Rails.root, 'test_invalid.csv')) }
    finished true
  end

  factory :sat_engine_sat_upload_without_csv, :class => 'SatEngine::SatUpload' do
    finished false
  end
end
