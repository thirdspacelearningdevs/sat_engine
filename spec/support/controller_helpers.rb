module ControllerHelpers
  def create_user(type = 'user')
    user = FactoryGirl.build(type)
    user.skip_confirmation!
    user.save
    user
  end

  def sign_in_user(user)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    sign_in :user, user
  end
end