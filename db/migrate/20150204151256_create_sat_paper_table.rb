class CreateSatPaperTable < ActiveRecord::Migration
  def change
    create_table :sat_engine_sat_papers do |t|
      t.integer :user_id
      t.string :last_selected_navigation_tab
      t.boolean :finished, :default => false
      t.string :pdf_path
      t.boolean :deleted, :default => false
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
