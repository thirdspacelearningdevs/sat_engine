module SatEngine
  class SatPaper < ActiveRecord::Base
    # user_id - The user who is generating the pdf document.
    # last_selected_navigation_tab - this field will be used then we will need go back from preview question to navigation
    # finished - boolean value for checking if pdf document have been generated.
    # pdf_path - to hold the path of the pdf. I currently don't a lot of details about it.
    attr_accessible :user_id, :last_selected_navigation_tab, :finished, :pdf_path, :deleted, :deleted_at
    has_many :selected_questions, dependent: :destroy

    mount_uploader :file, SatPaperUploader

	  def request_pdf
      # should be abstracted to Gem wrapper ThirdSpaceApi
	  	url = "#{Settings.third_space_api_url}sat_engine/sat_papers/#{self.id}"
			res = remote_request(url)

	  	if (res).present?
		    response = JSON.parse(res)
		  else
		  	# REMOVE When API finished - response MUST always be returned
		  	response = { success: false }
		  end
	  end

		def remote_request url
		  if url.present?
		    uri = URI.parse(url.to_s)
		    req = Net::HTTP::Get.new(uri.request_uri)
		    req['Authorization'] = "Token token=#{Settings.thirdspacelearning_token}"

		    # API should NEVER timeout!
				begin
			    res = Net::HTTP.start(uri.host, uri.port) {|http| 
			      http.request(req) 
			    }.body
			  rescue Net::ReadTimeout
			  	logger.warn("Net::ReadTimeout ::TIMESTAMP::#{Time.zone.now} No response from host '#{uri.host}'")
			  	nil
			  end
		  end
		end

		def remote_pdf_file url
			begin
				pdf = open(url)
				return pdf.read unless pdf.nil?
			rescue Net::ReadTimeout
			  	logger.warn("Net::ReadTimeout ::TIMESTAMP::#{Time.zone.now} Can not access file on '#{url}'")
			  	nil
		  end
		end

    def upload_file! content_string, original_format='pdf'
      begin
        path = "tmp/#{self.id}_paper.#{original_format}"
        file = File.new(path, "w")
        file.write(content_string.force_encoding("UTF-8"))

        self.file = file
        self.save!
        File.delete(file)
      rescue IOError
       logger.warn("Write Error ::TIMESTAMP::#{Time.zone.now} Can not write file to path #{path}")
       nil
      rescue Net::ReadTimeout
       logger.warn("Upload Error ::TIMESTAMP::#{Time.zone.now} Can not upload file to remote server")
       nil
      end
    end

		def to_pdf html
		  pdf = WickedPdf.new.pdf_from_string(html,
		        :margin => {
		          :top => 0,
		          :bottom => 0,
		          :left => 0,
		          :right => 0,
		        },

		        :lowquality => false,
		        :disable_internal_links => false,
		        :disable_external_links => false,
		        :page_height => 254,
		        :page_width => 191,
		        :print_media_type => true,
		        :orientation => 'Landscape',
		        :no_background => false,
		        :image => true
		      )
		end

  end
end