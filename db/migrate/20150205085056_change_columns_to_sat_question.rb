class ChangeColumnsToSatQuestion < ActiveRecord::Migration
  def up
  	rename_column :sat_engine_sat_questions, :papers, :paper
  end

  def down
  	rename_column :sat_engine_sat_questions, :paper, :papers
  end
end
