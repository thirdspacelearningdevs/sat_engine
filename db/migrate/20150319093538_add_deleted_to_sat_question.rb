class AddDeletedToSatQuestion < ActiveRecord::Migration
  def change
    add_column :sat_engine_sat_questions, :deleted, :boolean, :default => false
    add_column :sat_engine_sat_questions, :deleted_at, :datetime, :after => :deleted
  end
end