class ChangeAlternativeGranuleToGranuleAlternative < ActiveRecord::Migration
  def change
    rename_table :sat_engine_alternative_sat_question_granules, :sat_engine_granule_sat_question_alternatives
  end 
end
