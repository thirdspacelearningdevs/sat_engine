require "spec_helper"
require "shoulda-matchers"

describe SatEngine::SatQuestion do
  subject { FactoryGirl.create(:sat_engine_sat_question) }

  it { expect(subject).to be_valid }
  it { expect(subject).to have_many(:granule_sat_question_alternatives) }
  it { expect(subject).to have_many(:selected_questions) }

  describe "#question_file" do
    context "with valid path" do
      it "retrieve question file" do
        question = subject.question_file
        expect(question).not_to be_nil
      end
    end

	  context "with invalid path" do
  		it "return nil" do
  			subject.update_attribute(:question_code, "invalid_code")
				subject.reload
  			question = subject.question_file
  			expect(question).to be_nil
	  	end
	  end
	end

  describe "#mark_scheme_file" do
    context "with valid path" do
      it "retrieve question file" do
        mark_scheme = subject.mark_scheme_file
        expect(mark_scheme).not_to be_nil
      end
    end

    context "with invalid path" do
      it "retrieve question file" do
        subject.update_attribute(:mark_scheme_code, "invalid_code")
        subject.reload
        mark_scheme = subject.mark_scheme_file
        expect(mark_scheme).to be_nil
      end
    end
  end

  describe ".all_questions_for_granule_hash" do
    context "with valid garnule_position" do
      before(:each) do
        @granule = FactoryGirl.create(:granule, position: 2)
      end
      it "returns best match sat questions for specified granule" do
        subject.update_attribute(:granule_position, @granule.position)
        subject.reload
        sat_questions = SatEngine::SatQuestion.all_questions_for_granule_hash(@granule.position)
        expect(sat_questions[:best_match].first).to eq subject
      end

      it "returns alternative granules for specified granule" do
        FactoryGirl.create(:granule_sat_question_alternative, granule_position: @granule.position, sat_question_id: subject.id)
        sat_questions = SatEngine::SatQuestion.all_questions_for_granule_hash(@granule.position)
        expect(sat_questions[:alternatives].first).to eq subject
      end

      it "returns granule name" do
        sat_questions = SatEngine::SatQuestion.all_questions_for_granule_hash(@granule.position)
        expect(sat_questions[:granule_name]).to eq @granule.name
      end
    end

    context "with invalid granule position" do
      it 'returns nil' do
        invalid_granule_position = 12345
        sat_questions = SatEngine::SatQuestion.all_questions_for_granule_hash(invalid_granule_position)
        expect(sat_questions).to be nil
      end
    end
  end

  describe ".mark_as_destroyed!" do
    it 'record deleted state and time' do
      t = Time.local(2014, 4, 14, 14, 0, 0)
      Timecop.travel(t)

      subject.mark_as_destroyed!
      expect(subject.deleted).to be true
      expect(subject.deleted_at.to_s).not_to be Time.zone.now.to_s
    end

    it 'remove question from sat questions' do
      subject.mark_as_destroyed!
      expect(SatEngine::SatQuestion.available.all).not_to include(subject)
    end
  end

  describe ".mark_as_destroyed_all!" do
    it 'destroy all sat questions' do
      create(:sat_engine_sat_question)
      SatEngine::SatQuestion.mark_as_destroyed_all!
      expect(SatEngine::SatQuestion.available.all).to be_empty
    end
  end
end