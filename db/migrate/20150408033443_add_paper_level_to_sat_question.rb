class AddPaperLevelToSatQuestion < ActiveRecord::Migration
  def change
    add_column :sat_engine_sat_questions, :paper_level, :integer
  end
end
