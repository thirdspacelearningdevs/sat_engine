class CreateSatEngineAlternativeSatQuestionGranules < ActiveRecord::Migration
  def change
    create_table :sat_engine_alternative_sat_question_granules do |t|
      t.integer :granule_id
      t.integer :sat_question_id

      t.timestamps
    end
  end
end
