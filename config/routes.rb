SatEngine::Engine.routes.draw do

  resources :sat_questions, only: [:questions_for_granule] do
    member do
      get :preview
      get :show_image
    end
    collection do 
      get :questions_for_granule
    end
  end

  resources :sat_uploads do
    member do
      get :load_sat_questions
    end
  end
  
  resources :sat_papers do
    collection do
      get :curriculum_navigation
      get :year_navigation
      get :level_navigation
    end
  end

  get '/terms_and_conditions' => 'dashboard#terms_and_conditions', :as => :terms_and_conditions
  get '/privacy_policy' => 'dashboard#privacy_policy', :as => :privacy_policy

  root :to => 'dashboard#index'
end
