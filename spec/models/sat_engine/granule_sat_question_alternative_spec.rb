require "spec_helper"
require "shoulda-matchers"

describe SatEngine::GranuleSatQuestionAlternative do
  subject { FactoryGirl.build(:granule_sat_question_alternative) }
  it { expect(subject).to be_valid }
  it { expect(subject).to belong_to(:alternative_sat_questions)}

  describe ".mark_as_destroyed!" do
    it 'record deleted state and time' do
      t = Time.local(2014, 4, 14, 14, 0, 0)
      Timecop.travel(t)

      subject.mark_as_destroyed!
      expect(subject.deleted).to be true
      expect(subject.deleted_at.to_s).not_to be Time.zone.now.to_s
    end

    it 'remove question from sat questions' do
      subject.mark_as_destroyed!
      expect(SatEngine::GranuleSatQuestionAlternative.available.all).not_to include(subject)
    end
  end

  describe ".mark_as_destroyed_all!" do
    it 'destroy all sat questions' do
      create(:granule_sat_question_alternative)
      SatEngine::GranuleSatQuestionAlternative.mark_as_destroyed_all!
      expect(SatEngine::GranuleSatQuestionAlternative.available.all).to be_empty
    end
  end

end