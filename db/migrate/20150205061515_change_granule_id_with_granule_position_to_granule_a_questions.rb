class ChangeGranuleIdWithGranulePositionToGranuleAQuestions < ActiveRecord::Migration
  def up
  	rename_column :sat_engine_granule_sat_question_alternatives, :granule_id, :granule_position
  end

  def down
  	rename_column :sat_engine_granule_sat_question_alternatives, :granule_position, :granule_id
  end
end
