# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150319095657) do

  create_table "academic_calendars", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "academic_years", :force => true do |t|
    t.integer  "academic_calendar_id"
    t.integer  "year"
    t.date     "autumn_term_start"
    t.date     "autumn_term_end"
    t.date     "spring_term_start"
    t.date     "spring_term_end"
    t.date     "summer_term_start"
    t.date     "summer_term_end"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "account_managers", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.string   "picture"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "achieved_steps_to_success", :force => true do |t|
    t.integer  "feedback_granule_id"
    t.integer  "step_to_success_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.boolean  "achieved"
    t.integer  "state"
  end

  add_index "achieved_steps_to_success", ["feedback_granule_id"], :name => "index_achieved_steps_to_success_on_feedback_granule_id"
  add_index "achieved_steps_to_success", ["step_to_success_id"], :name => "index_achieved_steps_to_success_on_step_to_success_id"

  create_table "alternative_granule_questions", :force => true do |t|
    t.integer  "granule_id"
    t.integer  "sat_questions_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "audio_connection_states", :force => true do |t|
    t.string   "state"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "field"
    t.string   "description"
  end

  create_table "audio_status_states", :force => true do |t|
    t.string   "state"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "description"
  end

  create_table "black_marks", :force => true do |t|
    t.integer  "school_id"
    t.integer  "tutor_id"
    t.string   "reason"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "black_marks", ["school_id", "tutor_id"], :name => "index_black_marks_on_school_and_tutor", :unique => true
  add_index "black_marks", ["tutor_id"], :name => "index_black_marks_on_tutor_id"

  create_table "book_demos", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "school"
    t.string   "contact_number"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "booked_sessions", :force => true do |t|
    t.integer  "booking_id"
    t.integer  "booked_sitting_id"
    t.integer  "learning_session_id"
    t.boolean  "deleted",             :default => false
    t.datetime "deleted_at"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "booked_sessions", ["booked_sitting_id"], :name => "index_booked_sessions_on_booked_sitting_id"
  add_index "booked_sessions", ["booking_id"], :name => "index_booked_sessions_on_booking_id"
  add_index "booked_sessions", ["learning_session_id"], :name => "index_booked_sessions_on_learning_session_id"

  create_table "booked_sittings", :force => true do |t|
    t.integer  "booking_id"
    t.integer  "teacher_id"
    t.datetime "time"
    t.datetime "time2"
    t.integer  "number_of_tutors_requested"
    t.boolean  "deleted",                      :default => false
    t.datetime "deleted_at"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.boolean  "sent",                         :default => false
    t.integer  "live_sessions_events_counter", :default => 0
    t.integer  "school_staff_member_id"
  end

  add_index "booked_sittings", ["booking_id"], :name => "index_booked_sittings_on_booking_id"
  add_index "booked_sittings", ["deleted", "time"], :name => "index_booked_sittings_on_deleted_and_time"
  add_index "booked_sittings", ["teacher_id"], :name => "index_booked_sittings_on_teacher_id"

  create_table "bookings", :force => true do |t|
    t.integer  "user_id"
    t.integer  "purchase_info_id"
    t.datetime "begin_on"
    t.integer  "weeks_requested"
    t.integer  "students_requested"
    t.boolean  "deleted",            :default => false
    t.datetime "deleted_at"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
  end

  add_index "bookings", ["purchase_info_id"], :name => "index_bookings_on_purchase_info_id"
  add_index "bookings", ["user_id"], :name => "index_bookings_on_user_id"

  create_table "capacity_change_types", :force => true do |t|
    t.text     "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "capacity_changes", :force => true do |t|
    t.date     "from_date"
    t.date     "to_date"
    t.time     "from_time"
    t.time     "to_time"
    t.integer  "capacity_change_type_id"
    t.string   "reason_for_change"
    t.integer  "tutor_id"
    t.integer  "changer_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "capacity_changes", ["capacity_change_type_id"], :name => "index_capacity_changes_on_capacity_change_type_id"
  add_index "capacity_changes", ["tutor_id"], :name => "index_capacity_changes_on_tutor_id"

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "contact_forms", :force => true do |t|
    t.string   "type"
    t.string   "name"
    t.string   "email"
    t.string   "school"
    t.string   "phone"
    t.text     "comments"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "contact_forms", ["id", "type"], :name => "index_contact_forms_on_id_and_type"

  create_table "curriculum_categories", :force => true do |t|
    t.integer  "curriculum_level_id"
    t.string   "name"
    t.string   "path"
    t.string   "directory"
    t.integer  "position"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "curriculum_categories", ["curriculum_level_id"], :name => "index_curriculum_categories_on_curriculum_level_id"

  create_table "curriculum_levels", :force => true do |t|
    t.integer  "curriculum_type_id"
    t.string   "name"
    t.integer  "level"
    t.integer  "position"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "curriculum_levels", ["curriculum_type_id"], :name => "index_curriculum_levels_on_curriculum_type_id"

  create_table "curriculum_topics", :force => true do |t|
    t.integer  "curriculum_category_id"
    t.string   "name"
    t.string   "directory"
    t.integer  "position"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  add_index "curriculum_topics", ["curriculum_category_id"], :name => "index_curriculum_topics_on_curriculum_category_id"

  create_table "curriculum_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "delayed_exports", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "time"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.text     "emails"
    t.text     "generate_path"
    t.boolean  "sent",             :default => false
    t.text     "export_params"
    t.datetime "generated_at"
    t.boolean  "archived",         :default => false
    t.string   "export_data_path"
    t.boolean  "locked",           :default => false
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "echo_tests", :force => true do |t|
    t.string   "kosmos_test_id"
    t.date     "kosmos_last_tested"
    t.boolean  "kosmos_tested_ok"
    t.string   "kosmos_test_recording_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "events", :force => true do |t|
    t.string   "learning_session_id"
    t.integer  "user_id"
    t.integer  "granule_id"
    t.text     "command"
    t.boolean  "internal",            :default => false
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "events", ["granule_id"], :name => "index_events_on_granule_id"
  add_index "events", ["learning_session_id"], :name => "index_events_on_learning_session_id"
  add_index "events", ["user_id"], :name => "index_events_on_user_id"

  create_table "feedback_granules", :force => true do |t|
    t.integer  "session_feedback_id"
    t.integer  "granule_id"
    t.boolean  "deleted",             :default => false
    t.datetime "deleted_at"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.text     "academic_comment"
  end

  add_index "feedback_granules", ["granule_id"], :name => "index_feedback_granules_on_granule_id"
  add_index "feedback_granules", ["session_feedback_id"], :name => "index_feedback_granules_on_session_feedback_id"

  create_table "granules", :force => true do |t|
    t.integer  "curriculum_topic_id"
    t.string   "name"
    t.text     "description"
    t.string   "level"
    t.string   "directory"
    t.string   "filename"
    t.string   "km_static_url"
    t.integer  "position"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.string   "sublevel"
    t.string   "challenge_rating"
    t.integer  "next_granule_position"
    t.integer  "publish_year"
    t.boolean  "live"
    t.integer  "page_count"
  end

  add_index "granules", ["curriculum_topic_id"], :name => "index_granules_on_curriculum_topic_id"

  create_table "headset_purchases", :force => true do |t|
    t.text     "description"
    t.integer  "default_price"
    t.integer  "set_price"
    t.integer  "number_of_students"
    t.integer  "purchaser_id"
    t.integer  "school_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "headset_purchases", ["purchaser_id"], :name => "index_headset_purchases_on_purchaser_id"
  add_index "headset_purchases", ["school_id"], :name => "index_headset_purchases_on_school_id"

  create_table "holidays", :force => true do |t|
    t.string   "type"
    t.string   "country_symbol", :default => "uk"
    t.boolean  "global",         :default => false
    t.date     "holiday_date"
    t.integer  "school_id"
    t.string   "name"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  add_index "holidays", ["id", "type"], :name => "index_holidays_on_id_and_type"
  add_index "holidays", ["school_id"], :name => "index_holidays_on_school_id"

  create_table "internal_tutor_notes", :force => true do |t|
    t.integer  "student_id"
    t.integer  "author_id"
    t.string   "author_full_name"
    t.integer  "changer_id"
    t.string   "changer_full_name"
    t.string   "text"
    t.boolean  "deleted",           :default => false
    t.datetime "deleted_at"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
  end

  add_index "internal_tutor_notes", ["author_id"], :name => "index_internal_tutor_notes_on_author_id"
  add_index "internal_tutor_notes", ["changer_id"], :name => "index_internal_tutor_notes_on_changer_id"
  add_index "internal_tutor_notes", ["student_id"], :name => "index_internal_tutor_notes_on_student_id"

  create_table "knowledge_resource_granule_assets", :force => true do |t|
    t.integer  "knowledge_resource_granule_id"
    t.string   "file"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "knowledge_resource_granule_assets", ["knowledge_resource_granule_id"], :name => "knowledge_res_granule_assets"

  create_table "knowledge_resource_granules", :force => true do |t|
    t.integer  "knowledge_resource_id"
    t.string   "title"
    t.integer  "level"
    t.text     "description"
    t.integer  "see_also_id"
    t.integer  "easier_suggestion_id"
    t.integer  "harder_suggest"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "knowledge_resource_granules", ["knowledge_resource_id"], :name => "index_knowledge_resource_granules_on_knowledge_resource_id"

  create_table "knowledge_resources", :force => true do |t|
    t.string   "authority"
    t.string   "subject"
    t.string   "category"
    t.string   "topic"
    t.string   "granule"
    t.integer  "level"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "learning_group_users", :force => true do |t|
    t.integer  "user_id"
    t.integer  "learning_group_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "learning_group_users", ["learning_group_id"], :name => "index_learning_group_users_on_learning_group_id"
  add_index "learning_group_users", ["user_id", "learning_group_id"], :name => "index_learning_group_users_on_user_id_and_learning_group_id"
  add_index "learning_group_users", ["user_id"], :name => "index_learning_group_users_on_user_id"

  create_table "learning_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "owner_id"
  end

  add_index "learning_groups", ["owner_id"], :name => "index_learning_groups_on_owner_id"

  create_table "learning_session_events", :force => true do |t|
    t.integer  "user_id"
    t.string   "event_type"
    t.string   "state"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "action"
    t.integer  "learning_session_id"
  end

  add_index "learning_session_events", ["learning_session_id"], :name => "index_learning_session_events_on_learning_session_id"
  add_index "learning_session_events", ["user_id"], :name => "index_learning_session_events_on_user_id"

  create_table "learning_session_logged_users", :force => true do |t|
    t.integer  "learning_session_id"
    t.integer  "user_id"
    t.text     "tokbox_token"
    t.text     "voip"
    t.text     "historical_voip"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "learning_session_logged_users", ["learning_session_id"], :name => "index_learning_session_logged_users_on_learning_session_id"
  add_index "learning_session_logged_users", ["user_id"], :name => "index_learning_session_logged_users_on_user_id"

  create_table "learning_session_reassignments", :force => true do |t|
    t.integer  "learning_session_id"
    t.integer  "author_id"
    t.integer  "original_tutor_id"
    t.string   "original_tutor_full_name"
    t.integer  "new_tutor_id"
    t.string   "new_tutor_full_name"
    t.integer  "school_id"
    t.integer  "student_id"
    t.string   "student_full_name"
    t.integer  "reassigned_session_count"
    t.string   "reason_for_change"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "changed_sessions"
  end

  add_index "learning_session_reassignments", ["author_id"], :name => "index_learning_session_reassignments_on_author_id"
  add_index "learning_session_reassignments", ["learning_session_id"], :name => "index_learning_session_reassignments_on_learning_session_id"
  add_index "learning_session_reassignments", ["new_tutor_id"], :name => "index_learning_session_reassignments_on_new_tutor_id"
  add_index "learning_session_reassignments", ["school_id"], :name => "index_learning_session_reassignments_on_school_id"
  add_index "learning_session_reassignments", ["student_id"], :name => "index_learning_session_reassignments_on_student_id"

  create_table "learning_session_tutor_changes", :force => true do |t|
    t.integer  "capacity_change_id"
    t.integer  "learning_session_id"
    t.integer  "assigned_tutor_id"
    t.boolean  "resolved",            :default => false
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "learning_session_tutor_changes", ["capacity_change_id"], :name => "index_learning_session_tutor_changes_on_capacity_change_id"
  add_index "learning_session_tutor_changes", ["learning_session_id"], :name => "index_learning_session_tutor_changes_on_learning_session_id"

  create_table "learning_sessions", :force => true do |t|
    t.datetime "time"
    t.datetime "time2"
    t.integer  "teacher_id"
    t.boolean  "deleted",                      :default => false
    t.boolean  "test_session",                 :default => false
    t.text     "comments"
    t.text     "delete_webex_response"
    t.datetime "deleted_at"
    t.date     "date"
    t.integer  "knowledge_resource_id"
    t.datetime "start_time"
    t.string   "state",                        :default => "new"
    t.integer  "granule_id"
    t.string   "granule_path"
    t.string   "granule_name"
    t.datetime "granule_select_time"
    t.text     "comms_data"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.boolean  "started_by_student",           :default => false
    t.boolean  "started_by_tutor",             :default => false
    t.boolean  "completed_by_student",         :default => false
    t.boolean  "completed_by_tutor",           :default => false
    t.string   "student_url",                  :default => "#"
    t.string   "tutor_url",                    :default => "#"
    t.string   "webex_key"
    t.string   "kosmos_id"
    t.string   "tokbox_session_id"
    t.datetime "tutor_start_session_at"
    t.datetime "student_start_session_at"
    t.boolean  "session_ended",                :default => false
    t.string   "tokbox_archive_id"
    t.boolean  "recording_archived"
    t.integer  "number_of_recordings"
    t.date     "last_session_event"
    t.integer  "tutor_level"
    t.integer  "classroom_technology_id"
    t.text     "classroom_technology_session"
    t.string   "webex_tutor"
    t.string   "event_table_name"
    t.integer  "position"
  end

  add_index "learning_sessions", ["granule_id"], :name => "index_learning_sessions_on_granule_id"
  add_index "learning_sessions", ["knowledge_resource_id"], :name => "index_learning_sessions_on_knowledge_resource_id"
  add_index "learning_sessions", ["teacher_id"], :name => "index_learning_sessions_on_teacher_id"
  add_index "learning_sessions", ["time", "deleted"], :name => "index_learning_sessions_time_deleted"
  add_index "learning_sessions", ["time", "time2", "deleted"], :name => "index_learning_sessions_time_time2_deleted"

  create_table "live_session_feedback_events", :force => true do |t|
    t.integer  "learning_session_id"
    t.string   "field"
    t.string   "state"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "live_session_feedback_events", ["learning_session_id", "field"], :name => "lsfe_learning_session_id_field"
  add_index "live_session_feedback_events", ["learning_session_id"], :name => "lsfe_learning_session_id"

  create_table "local_authorities", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "offer_registrations", :force => true do |t|
    t.integer  "offer_id"
    t.string   "name"
    t.string   "position"
    t.string   "email"
    t.string   "school"
    t.string   "number_of_students"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "contact_number"
    t.string   "start_date"
    t.string   "best_time_to_speak"
    t.string   "package_name"
  end

  add_index "offer_registrations", ["offer_id"], :name => "index_offer_registrations_on_offer_id"

  create_table "offers", :force => true do |t|
    t.string   "school_name"
    t.text     "quote_teacher"
    t.text     "description"
    t.text     "price_text"
    t.string   "offer"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "package_timeslot_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "package_types", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "purchase_infos", :force => true do |t|
    t.integer  "school_id"
    t.integer  "number_of_students"
    t.float    "default_price"
    t.float    "set_price"
    t.text     "reason_for_price_change"
    t.integer  "number_of_weeks"
    t.boolean  "used",                     :default => false
    t.date     "start_date"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.date     "credit_expiry_date"
    t.string   "description"
    t.integer  "purchaser_id"
    t.string   "type"
    t.boolean  "deleted",                  :default => false
    t.datetime "deleted_at"
    t.integer  "package_type_id"
    t.integer  "package_timeslot_type_id"
    t.integer  "number_of_free_students",  :default => 0
    t.string   "default_total_price"
    t.string   "default_set_price"
  end

  add_index "purchase_infos", ["id", "type"], :name => "index_purchase_infos_on_id_and_type"
  add_index "purchase_infos", ["purchaser_id"], :name => "index_purchase_infos_on_purchaser_id"
  add_index "purchase_infos", ["school_id"], :name => "index_purchase_infos_on_school_id"

  create_table "purchase_renewals", :force => true do |t|
    t.string   "user_full_name"
    t.integer  "number_of_students"
    t.string   "time_slot"
    t.integer  "teacher_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "package"
  end

  create_table "purchase_supply_centers", :force => true do |t|
    t.integer  "purchase_info_id"
    t.integer  "supply_center_id"
    t.boolean  "selected"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "purchase_supply_centers", ["supply_center_id"], :name => "index_purchase_supply_centers_on_supply_center_id"

  create_table "rails_admin_histories", :force => true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      :limit => 2
    t.integer  "year",       :limit => 8
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], :name => "index_rails_admin_histories"

  create_table "recordings", :force => true do |t|
    t.integer  "learning_session_id"
    t.string   "file"
    t.string   "webex_key"
    t.string   "tutor_username"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.text     "webex_response"
    t.datetime "session_time"
    t.datetime "webex_created_at"
  end

  create_table "renewal_cost_calculation_records", :force => true do |t|
    t.integer  "number_of_students"
    t.string   "time_slot"
    t.string   "package"
    t.integer  "school_id"
    t.integer  "school_staff_member_id"
    t.boolean  "sent",                   :default => false
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  create_table "sat_engine_granule_sat_question_alternatives", :force => true do |t|
    t.integer  "granule_position"
    t.integer  "sat_question_id"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.boolean  "deleted",          :default => false
    t.datetime "deleted_at"
  end

  create_table "sat_engine_sat_papers", :force => true do |t|
    t.integer  "user_id"
    t.string   "last_selected_navigation_tab"
    t.boolean  "finished",                     :default => false
    t.string   "pdf_path"
    t.boolean  "deleted",                      :default => false
    t.datetime "deleted_at"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.string   "file"
  end

  create_table "sat_engine_sat_questions", :force => true do |t|
    t.integer  "year"
    t.string   "paper"
    t.string   "question"
    t.integer  "marks"
    t.string   "question_code"
    t.string   "mark_scheme_code"
    t.integer  "level"
    t.integer  "granule_position"
    t.integer  "alternative_granule_position"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.boolean  "deleted",                      :default => false
    t.datetime "deleted_at"
  end

  create_table "sat_engine_sat_uploads", :force => true do |t|
    t.boolean  "finished",   :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.string   "csv_file"
  end

  create_table "sat_engine_selected_questions", :force => true do |t|
    t.string   "title"
    t.integer  "sat_paper_id"
    t.integer  "sat_question_id"
    t.integer  "position"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "school_roles", :force => true do |t|
    t.string   "role_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "school_staff_members", :force => true do |t|
    t.integer  "school_id"
    t.string   "position_at_school"
    t.string   "phone"
    t.datetime "last_login_time"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.string   "preferred_contact_time_of_day"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.boolean  "show_fill_all_info_box",        :default => true
    t.boolean  "do_not_ask_me_again",           :default => false
    t.boolean  "deleted",                       :default => false
    t.datetime "deleted_at"
  end

  add_index "school_staff_members", ["school_id"], :name => "index_school_staff_members_on_school_id"

  create_table "school_staff_roles", :force => true do |t|
    t.integer  "school_staff_member_id"
    t.integer  "school_role_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  add_index "school_staff_roles", ["school_staff_member_id", "school_role_id"], :name => "index_school_staff_roles", :unique => true

  create_table "school_states", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "schools", :force => true do |t|
    t.boolean  "is_test_school",                      :default => false
    t.string   "name"
    t.text     "address"
    t.string   "postcode"
    t.string   "phone_number"
    t.integer  "local_authority_id"
    t.string   "head_teacher_first_name"
    t.string   "head_teacher_last_name"
    t.string   "head_teacher_email"
    t.string   "lead_teacher_first_name"
    t.string   "lead_teacher_last_name"
    t.string   "lead_teacher_email"
    t.string   "lead_teacher2_first_name"
    t.string   "lead_teacher2_last_name"
    t.string   "lead_teacher2_email"
    t.string   "finance_contact_name"
    t.string   "finance_contract_email"
    t.string   "ict_contact_name"
    t.string   "ict_contact_email"
    t.string   "ict_contact_phone"
    t.string   "it_technician_name"
    t.string   "it_technician_email"
    t.string   "it_technician_phone"
    t.string   "days_present"
    t.text     "comments"
    t.string   "time_zone",                           :default => "London"
    t.datetime "desired_time_for_session"
    t.integer  "desired_day_of_week_for_session"
    t.integer  "desired_number_of_weeks_for_session"
    t.text     "purchaser_details"
    t.boolean  "priority_school_flag",                :default => false
    t.integer  "number_of_headsets_ordered"
    t.float    "price_per_headset"
    t.datetime "created_at",                                                :null => false
    t.datetime "updated_at",                                                :null => false
    t.boolean  "deleted",                             :default => false
    t.datetime "deleted_at"
    t.float    "longitude"
    t.float    "latitude"
    t.boolean  "use_new_system",                      :default => false
    t.boolean  "accepted_terms_and_condidions",       :default => false
    t.string   "introduction_email"
    t.boolean  "migrated_school_staff",               :default => false
    t.datetime "welcome_page_visited_at"
    t.datetime "accepted_terms_and_conditions_at"
    t.boolean  "concern_flag",                        :default => false
    t.string   "other_networks"
    t.integer  "school_state_id"
    t.boolean  "basic_setup_completed",               :default => false
    t.datetime "basic_setup_completed_at"
    t.boolean  "it_setup_completed",                  :default => false
    t.datetime "it_setup_completed_at"
    t.integer  "classroom_technology_id",             :default => 1
    t.boolean  "renewal",                             :default => false
    t.string   "version",                             :default => "v2"
    t.integer  "account_manager_id"
  end

  add_index "schools", ["local_authority_id"], :name => "index_schools_on_local_authority_id"
  add_index "schools", ["school_state_id"], :name => "index_schools_on_school_state_id"

  create_table "session_attendees", :force => true do |t|
    t.integer  "learning_session_id"
    t.integer  "tutor_id"
    t.integer  "student_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "session_attendees", ["learning_session_id"], :name => "index_session_attendees_on_learning_session_id"
  add_index "session_attendees", ["student_id"], :name => "index_session_attendees_student_id"
  add_index "session_attendees", ["tutor_id"], :name => "index_session_attendees_tutor_id"

  create_table "session_availabilities", :force => true do |t|
    t.datetime "start_time"
    t.integer  "slot_in_day"
    t.date     "date"
    t.boolean  "slot_in_use",          :default => false
    t.boolean  "slot_is_locked",       :default => false
    t.integer  "slot_locked_by_id"
    t.datetime "slot_lock_expires_at"
    t.integer  "learning_session_id"
    t.integer  "parent_id"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.text     "properties"
    t.boolean  "use_summer_time"
  end

  add_index "session_availabilities", ["learning_session_id"], :name => "index_session_availabilities_on_learning_session_id"
  add_index "session_availabilities", ["parent_id"], :name => "index_session_availabilities_on_parent_id"
  add_index "session_availabilities", ["slot_locked_by_id"], :name => "index_session_availabilities_on_slot_locked_by_id"
  add_index "session_availabilities", ["start_time", "slot_in_use", "slot_is_locked", "slot_locked_by_id"], :name => "index_session_availabilities"

  create_table "session_availability_sitting_lists", :force => true do |t|
    t.integer  "session_availability_id"
    t.integer  "session_availability_sitting_id"
    t.integer  "user_id"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  create_table "session_availability_sittings", :force => true do |t|
    t.integer  "session_availability_id"
    t.integer  "number_of_tutors_max"
    t.integer  "number_of_tutors_current"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "session_events", :force => true do |t|
    t.integer  "session_lesson_id"
    t.float    "time"
    t.integer  "knowledge_resource_id"
    t.boolean  "connection_lost_from_student",     :default => false
    t.boolean  "connection_lost_from_tutor",       :default => false
    t.boolean  "connection_regained_from_student", :default => false
    t.boolean  "connection_regained_from_tutor",   :default => false
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
  end

  create_table "session_feedbacks", :force => true do |t|
    t.integer  "learning_session_id"
    t.integer  "user_id"
    t.boolean  "sent",                              :default => false
    t.text     "from_tutor_about_pupil"
    t.text     "from_tutor_about_pupil_private"
    t.integer  "student_topic_confident"
    t.text     "student_engagement"
    t.integer  "sound_quality"
    t.integer  "tutor_raiting"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.text     "student_feedback"
    t.text     "technical_issues"
    t.text     "recording_urls"
    t.text     "recording_response"
    t.boolean  "headphones_computer_raiting"
    t.boolean  "understand_tutor"
    t.boolean  "appropriate_lo",                    :default => false
    t.integer  "student_attitude"
    t.integer  "student_progress"
    t.integer  "effort_points"
    t.integer  "tutor_sound_quality"
    t.integer  "version",                           :default => 2
    t.string   "student_engagement_rate"
    t.integer  "student_effort_points"
    t.integer  "tutor_lo_selection_rate"
    t.integer  "tutor_session_end_time_in_minutes"
    t.boolean  "red_flag"
    t.string   "internal_tutor_notes"
    t.integer  "rate_your_teaching"
    t.string   "tutor_what_would_you_improve"
    t.string   "student_current_level"
    t.string   "student_target_level"
    t.integer  "student_hard_work_rate"
    t.integer  "suggested_lo_id"
    t.integer  "audio_status_state_id"
    t.integer  "audio_connection_state_id"
    t.integer  "workspace_status_state_id"
    t.string   "tech_notes"
  end

  add_index "session_feedbacks", ["audio_connection_state_id"], :name => "index_session_feedbacks_on_audio_connection_state_id"
  add_index "session_feedbacks", ["audio_status_state_id"], :name => "index_session_feedbacks_on_audio_status_state_id"
  add_index "session_feedbacks", ["learning_session_id"], :name => "index_session_feedbacks_on_learning_session_id"
  add_index "session_feedbacks", ["suggested_lo_id"], :name => "index_session_feedbacks_on_suggested_lo_id"
  add_index "session_feedbacks", ["workspace_status_state_id"], :name => "index_session_feedbacks_on_workspace_status_state_id"

  create_table "session_tutor_availabilities", :force => true do |t|
    t.integer  "session_availability_id"
    t.integer  "user_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "session_tutor_availabilities", ["session_availability_id"], :name => "index_session_tutor_availabilities_on_session_availability_id"
  add_index "session_tutor_availabilities", ["user_id"], :name => "index_session_tutor_availabilities_on_user_id"

  create_table "step_to_success_records", :force => true do |t|
    t.integer  "learning_session_id"
    t.integer  "student_id"
    t.integer  "tutor_id"
    t.integer  "step_number"
    t.integer  "granule_number"
    t.boolean  "achieved",            :default => false
    t.datetime "achieved_at"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.integer  "step_to_success_id"
  end

  add_index "step_to_success_records", ["learning_session_id"], :name => "index_step_to_success_records_on_learning_session_id"
  add_index "step_to_success_records", ["step_to_success_id"], :name => "index_step_to_success_records_on_step_to_success_id"

  create_table "steps_to_success", :force => true do |t|
    t.integer  "step_number"
    t.text     "description"
    t.integer  "granule_number"
    t.integer  "revise_granule"
    t.string   "revise_pages",   :default => "--- []\n"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "steps_to_success", ["granule_number"], :name => "index_steps_to_success_on_granule_number"

  create_table "subscriptions", :force => true do |t|
    t.string   "full_name"
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "supply_centers", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "time_slots", :force => true do |t|
    t.integer  "session_availability_id"
    t.integer  "day_of_week"
    t.integer  "day_of_month"
    t.integer  "week_within_year"
    t.integer  "month"
    t.integer  "year"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "time_slots", ["session_availability_id"], :name => "index_time_slots_on_session_availability_id"

  create_table "tutor_assets", :force => true do |t|
    t.integer  "tutor_id"
    t.string   "file"
    t.integer  "tutor_assets_type_id"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.boolean  "deleted",              :default => false
    t.datetime "deleted_at"
  end

  add_index "tutor_assets", ["tutor_assets_type_id"], :name => "index_tutor_assets_on_tutor_assets_type_id"
  add_index "tutor_assets", ["tutor_id"], :name => "index_tutor_assets_on_tutor_id"

  create_table "tutor_assets_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tutor_notes", :force => true do |t|
    t.integer  "granule_number"
    t.integer  "page"
    t.text     "content"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "tutor_notes", ["granule_number"], :name => "index_tutor_notes_on_granule_number"

  create_table "tutor_time_shifts", :force => true do |t|
    t.integer  "tutor_id"
    t.string   "day"
    t.boolean  "09_00",       :default => false
    t.boolean  "09_30",       :default => false
    t.boolean  "10_00",       :default => false
    t.boolean  "10_30",       :default => false
    t.boolean  "11_00",       :default => false
    t.boolean  "11_30",       :default => false
    t.boolean  "12_00",       :default => false
    t.boolean  "12_30",       :default => false
    t.boolean  "13_00",       :default => false
    t.boolean  "13_30",       :default => false
    t.boolean  "14_00",       :default => false
    t.boolean  "14_30",       :default => false
    t.boolean  "15_00",       :default => false
    t.boolean  "15_30",       :default => false
    t.boolean  "16_00",       :default => false
    t.boolean  "16_30",       :default => false
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.float    "total_hours"
  end

  add_index "tutor_time_shifts", ["tutor_id"], :name => "index_tutor_time_shifts_on_tutor_id"

  create_table "user_login_trackings", :force => true do |t|
    t.integer  "user_id"
    t.datetime "log_in_date"
    t.datetime "log_out_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_login_trackings", ["user_id"], :name => "index_user_login_trackings_on_user_id"

  create_table "users", :force => true do |t|
    t.integer  "parent_id"
    t.string   "type",                          :limit => 40
    t.string   "username",                      :limit => 60
    t.string   "real_name",                     :limit => 60
    t.string   "first_name",                    :limit => 40
    t.string   "last_name",                     :limit => 40
    t.string   "gender"
    t.string   "phone",                         :limit => 40
    t.text     "notes"
    t.boolean  "admin",                                       :default => false
    t.string   "email",                                       :default => "",       :null => false
    t.string   "company_name"
    t.string   "work_type"
    t.string   "encrypted_password",                          :default => "",       :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                               :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",                             :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.datetime "account_closed_on_date"
    t.boolean  "deleted",                                     :default => false
    t.datetime "deleted_at"
    t.datetime "created_at",                                                        :null => false
    t.datetime "updated_at",                                                        :null => false
    t.string   "time_zone",                                   :default => "London"
    t.boolean  "active",                                      :default => true
    t.string   "school_name"
    t.text     "description"
    t.integer  "school_id"
    t.boolean  "use_webex",                                   :default => true
    t.string   "ability"
    t.string   "engagement"
    t.string   "language"
    t.string   "year_group"
    t.text     "comments"
    t.string   "current_target_level"
    t.string   "target_level"
    t.float    "download_speed"
    t.float    "upload_speed"
    t.boolean  "skip_speed_test",                             :default => false
    t.datetime "start_time"
    t.datetime "end_time"
    t.boolean  "is_male"
    t.integer  "councillor_id"
    t.boolean  "archived",                                    :default => false
    t.datetime "archived_at"
    t.boolean  "logged"
    t.integer  "tutor_level",                                 :default => 1
    t.date     "date_of_birth"
    t.string   "tags"
    t.integer  "school_staff_member_id"
    t.boolean  "sen"
    t.text     "sen_comments"
    t.boolean  "eal"
    t.text     "eal_comments"
    t.string   "prefered_learning_style"
    t.string   "reading_confidence"
    t.integer  "memory_strength"
    t.boolean  "pupil_premium"
    t.string   "ks2_sat_result"
    t.boolean  "preferred_visual",                            :default => false
    t.boolean  "preferred_auditory",                          :default => false
    t.boolean  "preferred_kinesthetic",                       :default => false
    t.text     "comment"
    t.boolean  "use_kosmos",                                  :default => false
    t.integer  "classroom_technology",                        :default => 0
    t.integer  "work_on_hour",                                :default => 0
    t.boolean  "trained_in_webex",                            :default => false
    t.boolean  "trained_in_kosmos",                           :default => false
    t.datetime "current_target_level_date"
    t.datetime "target_level_date"
    t.integer  "supply_center_id"
    t.string   "position_at_school"
    t.string   "preferred_contact_time_of_day"
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["councillor_id"], :name => "index_users_on_councillor_id"
  add_index "users", ["id", "type"], :name => "index_users_on_id_and_type"
  add_index "users", ["parent_id"], :name => "index_users_on_parent_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["school_id"], :name => "index_users_on_school_id"
  add_index "users", ["school_staff_member_id"], :name => "index_users_on_school_staff_member_id"
  add_index "users", ["supply_center_id"], :name => "index_users_on_supply_center_id"
  add_index "users", ["type", "archived", "deleted_at", "active", "work_on_hour", "tutor_level", "trained_in_webex"], :name => "users_tutor_availability"
  add_index "users", ["type", "archived", "deleted_at", "active"], :name => "user_all_active"
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

  create_table "webex_accounts", :force => true do |t|
    t.integer  "supply_center_id"
    t.integer  "tutor_id"
    t.string   "username"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "password"
  end

  add_index "webex_accounts", ["supply_center_id"], :name => "index_webex_accounts_on_supply_center_id"
  add_index "webex_accounts", ["tutor_id"], :name => "index_webex_accounts_on_tutor_id"

  create_table "workspace_status_states", :force => true do |t|
    t.string   "state"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "description"
  end

end
