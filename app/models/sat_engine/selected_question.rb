module SatEngine
  class SelectedQuestion < ActiveRecord::Base
    # title - each question contains unique title according to navigation. 
    # For example, Centimetres( it means it was accessed throw the curriculum navigation )
    # sat_question_id - foreign key for SatQuestion
    # position - the position in which we will be putting questions to pdf
    attr_accessible :title, :sat_question_id, :position
    belongs_to :sat_paper
    belongs_to :sat_question
  end
end