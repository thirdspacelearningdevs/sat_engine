require "spec_helper"
require "shoulda-matchers"

describe SatEngine::SatUpload do
  subject { FactoryGirl.create(:sat_engine_sat_upload) }
  let(:upload_without_csv) {
    FactoryGirl.build(:sat_engine_sat_upload_without_csv)
  }

  it { expect(subject).to be_valid }
  it { expect(upload_without_csv).to validate_presence_of(:csv_file) }
end