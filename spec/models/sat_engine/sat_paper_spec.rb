require "spec_helper"
require "shoulda-matchers"

describe SatEngine::SatPaper do
  subject { FactoryGirl.build(:sat_engine_sat_paper) }
  it { expect(subject).to be_valid }
  it { expect(subject).to have_many(:selected_questions) }
end
