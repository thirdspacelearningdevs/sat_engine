class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def authenticate_admin!
    redirect_to main_app.home_path unless current_user.admin?
  end

  def authenticate_tutor_or_tutor_admin_or_admin_or_academic_councillor!
    redirect_to main_app.home_path unless current_user.academic_councillor? or current_user.tutor_admin? or current_user.admin? or current_user.tutor?
  end
end
