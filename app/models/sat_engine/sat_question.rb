module SatEngine
  class SatQuestion < ActiveRecord::Base
    attr_accessible :alternative_granule_position, :granule_position, :level, :mark_scheme_code, :marks, :paper, :question, :question_code, :year, :deleted, :deleted_at

    belongs_to :granule, foreign_key: :granule_position, primary_key: :position

    has_many :granule_sat_question_alternatives
    has_many :alternative_granules, class_name: 'Granule', through: :granule_sat_question_alternative, :source => :granule
    has_many :selected_questions

    scope :available, -> { where(:deleted => false) }

    scope :best_match_for_granule, ->(granule_position) { available.where("granule_position = ?", granule_position) }
    scope :alternatives_for_granule, ->(granule_position) { 
      available.joins(:granule_sat_question_alternatives).where("sat_engine_granule_sat_question_alternatives.granule_position = ?", granule_position) 
    }

    def self.all_questions_for_granule_hash granule_position
      granule = Granule.where(position: granule_position).first
      return if granule.blank?

      best_match = SatQuestion.best_match_for_granule(granule_position)
      alternatives = SatQuestion.alternatives_for_granule(granule_position)
      { granule_name: granule.name, best_match: best_match, alternatives: alternatives }
    end

    def question_file
      fetch_file(absolute_question_file_path)
    end

    def mark_scheme_file
      fetch_file(absolute_mark_scheme_file_path)
    end

    def absolute_question_file_path
      Settings.sat_papers_path+relative_question_image_path
    end

    def absolute_mark_scheme_file_path
      Settings.sat_papers_path+relative_mark_scheme_image_path
    end

    def self.mark_as_destroyed_all!
      SatQuestion.update_all(deleted: true, deleted_at: Time.zone.now)
    end

    def mark_as_destroyed!
      update_attribute(:deleted, true)
      update_attribute(:deleted_at, Time.zone.now)
    end

    def to_s
      "#{year} #{paper} - L#{paper_level} - Q #{question} -  #{marks} marks"
    end

    private
    def fetch_file path
      begin
        open(path, "rb").read
      rescue Exception => e
        logger.warn("Can not read sat paper located on path #{path}" + e.message)
        nil
      end
    end

    def relative_question_image_path
      "level#{paper_level}/#{year}/#{paper}/questions/#{question_code}"+".png"
    end

    def relative_mark_scheme_image_path
    	"level#{paper_level}/#{year}/#{paper}/mark_scheme/#{mark_scheme_code}"+".png"
    end
  end
end