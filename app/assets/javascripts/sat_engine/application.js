// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require sat_engine/sat_uploads
//= require sat_engine/sat_papers

//= require_tree .
//= require ./sat.js

jQuery(document).ready(function () {
    new Application();

    //expand collapse table
    $('.show-table').click(function (e) {
        e.preventDefault();

        var $this = $(this),
            $table = $this.prev('table');

        if ($table.hasClass('tab-expand')) {
            $table.removeClass('tab-expand').addClass('tab-collapse');
        } else {
            $table.removeClass('tab-collapse').addClass('tab-expand');
        }
    });

    //hide element
    $('[class*="message"] [class*="close"]').click(function (e) {
        e.preventDefault();

        $(this).parent().hide('slow'); //cookies?

    });

    //replace * label require
    $(".label-input label").each(function () {
        var $this = $(this),
            $text = $this.html();

        $text = $text.replace('*', '<span class="i-icon i-required">*</span>');
        $this.html($text);
    });

    //fullscreen button
    $('.fullscreen-icon').click(function (e) {
        e.preventDefault();

        $('body').toggleClass('fullscreen');
    });

    var bookingReload;

    if ($('body').hasClass('sat_engine/sat_papers-new')) {
        var sticky_toc_offset_top = $('.sat_sidebar').offset().top;
        var sticky_toc = function () {

            var sat_sidebar_height = $('.sat_sidebar').height();
            if (sat_sidebar_height > 850) {
                sat_sidebar_height = 150;
            } else {
                sat_sidebar_height = 0;
            }

            var scroll_top = $(window).scrollTop();
            var content_height = $('#content').height() - 400 - sat_sidebar_height;

            if (scroll_top > sticky_toc_offset_top) {
                $('.sat_sidebar').addClass('stick');
            } else {
                $('.sat_sidebar').removeClass('stick');
            }

            if (scroll_top > content_height) {
                $('.sat_sidebar').addClass('stick-end');
            } else {
                $('.sat_sidebar').removeClass('stick-end');
            }
        };

        $(window).scroll(function () {
            sticky_toc();
        });
    }

    $('tr.sat-curriculum-granule').livequery('click', function (e) {
        e.preventDefault();

        $('tr.sat-curriculum-granule').removeClass('selected');
        $(this).addClass('selected');
    });
});
