var SatPapers = $.inherit({
  __constructor : function() {
     $('.sat-curriculum-granule').livequery('click',$.delegate(this, this.loadMatchedQuestions));
     $('#table-draggable1 tr').livequery('dblclick',$.delegate(this, this.doubleClickMatchedSatQuestion));
     $('#table-draggable2 tr').livequery('dblclick',$.delegate(this, this.doubleClickSelectedSatQuestion));
     this.init_dragging_table();
  },

  loadMatchedQuestions: function(src, event){
  	var granule_position = src.dataset.granulePosition;
  	var path = '/sat_engine/sat_questions/questions_for_granule';

  	$.get(path, { granule_position: granule_position },function(data) {
			$('.sat-matched-questions').empty();
			$('.sat-matched-questions').append(data['content']);
    }, "json");
  },

  init_dragging_table: function(){
    var $tabs = $('#table-draggable2')
    $("tbody.connectedSortable")
        .sortable({
            connectWith: ".connectedSortable",
            // items: "> tr:not(:first)",
            items: "tr.sat-question-sortable",
            appendTo: $tabs,
            helper: "clone",
            zIndex: 999990
        })
        .disableSelection();

    var $tab_items = $(".nav-tabs > li", $tabs).droppable({
        accept: ".connectedSortable tr",
        hoverClass: "ui-state-hover",
        drop: function (event, ui) {
            return false;
        }
    });

  },
  
  doubleClickMatchedSatQuestion: function(src, event){
    if(!$(src).hasClass("question-block-header") && $(src).hasClass("sat-question-sortable")) {
        $('#table-draggable2').append(src);
    }
  },
  
  doubleClickSelectedSatQuestion: function(src, event){
    if(!$(src).hasClass("question-block-header")) {
      $('#table-draggable1').append(src);
    }
  }
  
 }, {

});

jQuery(document).ready(function() {
  new SatPapers();
});


