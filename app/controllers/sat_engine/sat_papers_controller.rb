module SatEngine
  class SatPapersController < SatEngine::ApplicationController
    before_filter :authenticate_user!
    before_filter :authenticate_tutor_or_tutor_admin_or_admin_or_academic_councillor!
    
    def new
      @sat_paper = SatPaper.new
    end

    def create
      @sat_paper = SatPaper.new
      unless params[:sat_questions].present?
        flash[:error] = I18n.t('sat_papers.error.no_selection')
        return respond_to do |format|
          format.html { redirect_to new_sat_paper_path }
        end
      end
      
      selected_questions = find_sat_paper_questions(@sat_paper, params[:sat_questions])
      if @sat_paper.save
        flash[:success] = I18n.t('sat_papers.success')

        respond_to do |format|          
          format.html do 
            redirect_to sat_paper_path(@sat_paper) 
          end

          format.pdf {
            pdf = paper_to_pdf
            render :text => pdf
          }
        end
      else
        flash[:error] = I18n.t('sat_papers.error.default')
        render :new
      end
    end

    def show
      @sat_paper = SatPaper.find(params[:id])
      
      # request pdf url from API
      response = @sat_paper.request_pdf
      @sat_paper_url = response['url']

      if @sat_paper_url.blank?
        flash[:error] = I18n.t('sat_papers.error.not_available')
        redirect_to main_app.home_path
      end
    end
    
    def curriculum_navigation
      if params[:filter]
        @granules = Granule.all_active_granules.filter(params[:filter], params[:value]).page(params[:page]).per(20)
        return render :json => {:content => render_to_string(:action => :curriculum_navigation_table)}
      end
      if params[:search]
        @term = params[:search]
        @granules = Granule.all_active_granules.search(params[:search]).page(params[:page]).per(20)
        return render :json => {:content => render_to_string(:action => :curriculum_navigation_table)}
      end
      @granules = Granule.all_active_granules.page(params[:page]).per(20)
      return respond_to do |format|
        format.html { render :layout => false }
        format.json { render :json => {:content => render_to_string(:action => :curriculum_navigation)} }
      end
    end
    
    def year_navigation
      render :layout => false
    end
    
    def level_navigation
      render :layout => false
    end

    private
    def find_sat_paper_questions sat_paper, sat_questions_params
      sat_questions = SatQuestion.where(id: sat_questions_params[:id]).order("field(id, #{sat_questions_params[:id].join(',')})")

      sat_questions.each_with_index do |sat_question, index|
        sat_paper.selected_questions.build(sat_question_id: sat_question.id, title: sat_questions_params[:granule_name][index])
      end
    end

    def upload_paper
      
    end

    # @deprecated [Supply link only by @sat_paper.request_pdf]
    def paper_to_pdf
      if Settings.generate_sat_paper_on_api
        response = @sat_paper.request_pdf
        pdf = response['success'] ? @sat_paper.remote_pdf_file(response['url']) : nil
      else
        html = render_to_string(:action => :show, :formats => [:pdf])
        pdf = @sat_paper.to_pdf(html)
      end
    end

  end
end