FactoryGirl.define do
  factory :user do
    sequence(:email) {|n| Faker::Internet.email }
    password Faker::Internet.password(8)
  end

  factory :academic_councillor, class: 'AcademicCouncillor', parent: :user do
    type "AcademicCouncillor"
  end

  factory :admin, class: 'Admin', parent: :user do
    type "Admin"
    admin true
  end

  factory :tutor, class: 'Tutor', parent: :user do
    type "Tutor"
  end

  factory :tutor_admin, class: 'TutorAdmin', parent: :user do
    type "TutorAdmin"
  end
end