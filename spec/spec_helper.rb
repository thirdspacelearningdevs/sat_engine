# include Devise::TestHelpers

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../dummy/config/environment", __FILE__)
require "pry"
require 'rspec/rails'
require 'rspec/autorun'
require 'faker'
require 'capybara/rspec'
require 'factory_girl_rails'  
require 'database_cleaner'
require "shoulda-matchers"
require 'devise'
require 'webmock/rspec'
require 'timecop'

# Show only what is visible
Capybara.ignore_hidden_elements = false

# Stubbing external requests
# https://robots.thoughtbot.com/how-to-stub-external-services-in-tests
# Ensure that test suite can’t make external requests
WebMock.disable_net_connect!(allow_localhost: true)

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
  Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }

RSpec.configure do |config|
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = "random"

  # Factory girl
  config.include FactoryGirl::Syntax::Methods

  # Bypass authentication
  config.include Devise::TestHelpers, :type => :controller
  config.include ControllerHelpers, :type => :controller

  config.after(:each) do
    FileUtils.rm_rf(Dir[Rails.root.join('public', 'uploads')])
  end

  # include paths from dummy app
  config.include Dummy::Application.routes.url_helpers

  # TODO capybara to devise
  # Load capybara features
  # config.include Features::SessionHelpers, type: :feature

  config.before(:suite) do
    DatabaseCleaner.strategy = :truncation
  end

  config.before :each do
    DatabaseCleaner.start
  end
end
